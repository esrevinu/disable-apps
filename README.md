This is an Android app to do communication with [disableAppDaemon] by a Unix 
domain socket. Thus disableAppDaemon is needed to be installed.

[disableAppDaemon]: https://gitlab.com/esrevinu/disableAppsDaemon

The purpose of this app is to do two things:

1. disable or enable apps
2. change battery charge limit

The second function is specific to some SONY phones. It is working for Xperia 
Z5 compact.

The main purpose is the first one. Apps can be disabled besides being
uninstalled. While uninstalling an app removes its data, disabling it does not.
However, there are no interfaces via which users disable apps in non-root.
This app provides an interface.

These practical actions are system stuff. So they are done by disableAppDaemon.
disableAppDaemon is needed to be installed by recovery work, and to be given
permissions to perform those things.
