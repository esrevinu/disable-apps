package org.duckdns.hunmaul.disableapps;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

public class AppFragment extends Fragment
        implements ConfirmFragment.ConfirmDialogListener,
        MyAppRecyclerViewAdapter.OnListInteractionListener
{

    private final String ADAPTER_SAVED = "adapter_saved";
    private final String APPINFO_SAVED = "appinfo_saved";
    private final String MODE_SAVED    = "mode_saved";

    private MyAppRecyclerViewAdapter mAdapter;

    private ArrayList<ApplicationInfo> mAppInfos;
    private MyAppRecyclerViewAdapter.MODE mMode;

    public AppFragment() {
        mAppInfos = new ArrayList<>();
        mMode = MyAppRecyclerViewAdapter.MODE.TO_DISABLE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_list, container, false);

        MyAppRecyclerViewAdapter adapter = null;
        if (savedInstanceState != null) {
            adapter = savedInstanceState.getParcelable(ADAPTER_SAVED);
            mAppInfos = savedInstanceState.getParcelableArrayList(APPINFO_SAVED);
            mMode = (MyAppRecyclerViewAdapter.MODE) savedInstanceState.getSerializable(MODE_SAVED);
        }

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            if (adapter == null) {
                // While initializing AppList singleton, show a progressbar
                // Give RecyclerViewAdaptor's constructor a null object to make the UI thread progress
                initializeAppList();
                mAdapter = new MyAppRecyclerViewAdapter((AppList)null, this);
                onListInteraction(null,
                        MyAppRecyclerViewAdapter.MODE.TO_DISABLE, false);
            } else {
                mAdapter = new MyAppRecyclerViewAdapter(adapter, this);
            }
            recyclerView.setAdapter(mAdapter);
        }
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onListInteraction(AppList.App item,
                                  MyAppRecyclerViewAdapter.MODE mode,
                                  boolean selected) {
        if (item == null) {
            mAppInfos.clear();
            mMode = mode;
            ActionBar actionBar = ((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
            if (actionBar != null)
                actionBar.setTitle(R.string.disable_apps);
            return;
        }
        if (mMode != mode) {
            mAppInfos.clear();
            mMode = mode;
            ActionBar actionBar = ((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar();
            if (actionBar != null) {
                if (mMode == MyAppRecyclerViewAdapter.MODE.TO_DISABLE) {
                    actionBar.setTitle(R.string.disable_apps);
                } else {
                    actionBar.setTitle(R.string.enable_apps);
                }
            }
        }

        if (selected) {
            mAppInfos.add(item.app_info);
        } else {
            mAppInfos.remove(item.app_info);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_app, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_disable_enable:
                if (mAppInfos.size() == 0) {
                    Toast.makeText(getContext(), "None selected",
                            Toast.LENGTH_SHORT).show();
                    return true;
                }
                String message;
                if (mMode == MyAppRecyclerViewAdapter.MODE.TO_DISABLE)
                    message = getResources().getQuantityString(R.plurals.confirm_da,
                            mAppInfos.size(), mAppInfos.size());
                else
                    message = getResources().getQuantityString(R.plurals.confirm_ea,
                            mAppInfos.size(), mAppInfos.size());
                DialogFragment dialog1 = ConfirmFragment.newInstance(message, this);
                dialog1.show(Objects.requireNonNull(getFragmentManager()), "ConfirmDialogFragment");
                return true;
            case R.id.action_refresh_apps_info:
                mAppInfos.clear();
                mAdapter.updateDataSet();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initializeAppList() {
        View progress = Objects.requireNonNull(getActivity()).findViewById(R.id.progressBar);

        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                progress.setVisibility(View.GONE);
                if (mAdapter != null) mAdapter.updateDataSet();
            }
        };

        Thread thread = new Thread() {
            @Override
            public void run() {
                AppList.getInstance(Objects.requireNonNull(getActivity()).getPackageManager());

                Message msg = Message.obtain();
                handler.sendMessage(msg);
            }
        };
        thread.start();
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        View progress = Objects.requireNonNull(getActivity()).findViewById(R.id.progressBar);

        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                String res = msg.getData().getString("Result");
                progress.setVisibility(View.GONE);
                mAppInfos.clear();
                mAdapter.updateDataSet();
                Toast.makeText(getContext(), res, Toast.LENGTH_SHORT).show();
            }
        };

        Thread thread = new Thread() {
            @Override
            public void run() {
                SocketWork socketWork = new SocketWork();
                String res = socketWork.sendDisableEnableApps(mMode == MyAppRecyclerViewAdapter.MODE.TO_DISABLE,
                        mAppInfos.stream().map(appinfo -> appinfo.packageName)
                                .collect(Collectors.toList()));
                Bundle bundle = new Bundle();
                bundle.putString("Result", res);
                Message msg = Message.obtain();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        thread.start();
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ADAPTER_SAVED, mAdapter);
        outState.putParcelableArrayList(APPINFO_SAVED, mAppInfos);
        outState.putSerializable(MODE_SAVED, mMode);
    }

}
