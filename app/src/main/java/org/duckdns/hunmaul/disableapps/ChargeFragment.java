package org.duckdns.hunmaul.disableapps;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ChargeFragment extends Fragment
        implements ConfirmFragment.ConfirmDialogListener
{

    private boolean llk_enabled;
    private int llk_max;
    private int llk_min;

    public ChargeFragment() {
        llk_enabled = true;
        llk_max = 85;
        llk_min = 50;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charge, container, false);

        View progress = Objects.requireNonNull(getActivity()).findViewById(R.id.progressBar);
        progress.setVisibility(View.VISIBLE);
        String res = new SocketWork().requestChageLimitSettings();
        progress.setVisibility(View.GONE);

        Pattern p = Pattern.compile("charge-limit (\\d+) (\\d+) (\\d+)");
        Matcher m = p.matcher(res);
        if (m.find()) {
            llk_enabled = Integer.parseInt(Objects.requireNonNull(m.group(1))) != 0;
            llk_max = Integer.parseInt(Objects.requireNonNull(m.group(2)));
            llk_min = Integer.parseInt(Objects.requireNonNull(m.group(3)));
        } else {
            Toast.makeText(getContext(), res, Toast.LENGTH_SHORT).show();
        }

        Switch sw_llk_enable = view.findViewById(R.id.switch_llk_enable);
        EditText et_llk_max = view.findViewById(R.id.charge_maximum);
        EditText et_llk_min = view.findViewById(R.id.charge_minimum);
        TextView tv_llk_max = view.findViewById(R.id.textview_charge_max);
        TextView tv_llk_min = view.findViewById(R.id.textview_charge_min);
        sw_llk_enable.setChecked(llk_enabled);
        if (llk_enabled) {
            et_llk_max.setEnabled(true);
            et_llk_min.setEnabled(true);
            tv_llk_max.setEnabled(true);
            tv_llk_min.setEnabled(true);
            et_llk_max.setText(String.valueOf(llk_max));
            et_llk_min.setText(String.valueOf(llk_min));
        } else {
            et_llk_max.setEnabled(false);
            et_llk_min.setEnabled(false);
            tv_llk_max.setEnabled(false);
            tv_llk_min.setEnabled(false);
        }

        sw_llk_enable.setOnCheckedChangeListener((v, isChecked) -> {
            llk_enabled = isChecked;
            if (isChecked) {
                et_llk_max.setEnabled(true);
                et_llk_min.setEnabled(true);
                tv_llk_max.setEnabled(true);
                tv_llk_min.setEnabled(true);
                et_llk_max.setText(String.valueOf(llk_max));
                et_llk_min.setText(String.valueOf(llk_min));
            } else {
                et_llk_max.setEnabled(false);
                et_llk_min.setEnabled(false);
                tv_llk_max.setEnabled(false);
                tv_llk_min.setEnabled(false);
            }
        });

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_charge, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_adjust_charge_limit:
                EditText et_llk_max = Objects.requireNonNull(getActivity()).findViewById(R.id.charge_maximum);
                EditText et_llk_min = getActivity().findViewById(R.id.charge_minimum);
                llk_max = Integer.parseInt(et_llk_max.getText().toString());
                llk_min = Integer.parseInt(et_llk_min.getText().toString());
                DialogFragment dialog2 = ConfirmFragment.newInstance(
                        getString(R.string.confirm_change_charge_settings),
                        this);
                assert getFragmentManager() != null;
                dialog2.show(getFragmentManager(), "ConfirmDialogFragment");
                return true;
            case R.id.action_refresh_battery_settings:
                ChargeFragment new_fragment = new ChargeFragment();
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.mainContent, new_fragment, MainActivity.FRAGMENT_MAIN)
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Activity context = getActivity();
        assert context != null;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.charge_limit_preference_file), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        View progress = context.findViewById(R.id.progressBar);

        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);

                String res = msg.getData().getString(getString(R.string.socketwork_result_key));
                View progress = context.findViewById(R.id.progressBar);
                progress.setVisibility(View.GONE);
                Toast.makeText(context, res, Toast.LENGTH_SHORT).show();
                editor.putBoolean(getString(R.string.saved_llk_enabled), llk_enabled);
                editor.putInt(getString(R.string.saved_llk_max), llk_max);
                editor.putInt(getString(R.string.saved_llk_min), llk_min);
                editor.apply();

                ChargeFragment new_fragment = new ChargeFragment();
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.mainContent, new_fragment, MainActivity.FRAGMENT_MAIN)
                        .commit();
            }
        };

        Thread thread = new Thread() {
            @Override
            public void run() {
                SocketWork socketWork = new SocketWork();
                String res = socketWork.sendChageLimitSettings(llk_enabled ? 1 : 0, llk_max, llk_min);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.socketwork_result_key), res);
                Message msg = Message.obtain();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };
        thread.start();
        progress.setVisibility(View.VISIBLE);

    }
}
