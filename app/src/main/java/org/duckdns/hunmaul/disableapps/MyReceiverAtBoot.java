package org.duckdns.hunmaul.disableapps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

public class MyReceiverAtBoot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            SharedPreferences sharedPref = context.getSharedPreferences(
                    context.getString(R.string.charge_limit_preference_file), Context.MODE_PRIVATE);
            boolean llk_enabled = sharedPref.getBoolean(context.getString(R.string.saved_llk_enabled), true);
            int llk_max = sharedPref.getInt(context.getString(R.string.saved_llk_max), -1);
            int llk_min = sharedPref.getInt(context.getString(R.string.saved_llk_min), -1);

            if (llk_max > 0) {
                SocketWork socketWork = new SocketWork();
                String res = socketWork.sendChageLimitSettings(llk_enabled ? 1 : 0, llk_max, llk_min);
                Toast.makeText(context, res, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
