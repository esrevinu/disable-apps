package org.duckdns.hunmaul.disableapps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity
{

    static final String FRAGMENT_MAIN = "MainFragment";

    private final String FRAGMENT_SAVED = "SavedFragment";

    ListView mDrawerList;
    ConstraintLayout mDrawerPane;
    private DrawerLayout mDrawerLayout;

    ArrayList<NavItem> mNavItems = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavItems.add(new NavItem(getString(R.string.app_mod), getString(R.string.da_desc),
                R.drawable.ic_settings_applications_black_24dp));
        mNavItems.add(new NavItem(getString(R.string.charge_limit), getString(R.string.cl_desc),
                R.drawable.ic_battery_unknown_black_24dp));
        mNavItems.add(new NavItem(getString(R.string.about), getString(R.string.ab_desc),
                R.drawable.ic_info_black_24dp));

        // DrawerLayout
        mDrawerLayout = findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = findViewById(R.id.drawerPane);
        mDrawerList = findViewById(R.id.navList);
        DrawerListAdapter adapter = new DrawerListAdapter(this, mNavItems);
        mDrawerList.setAdapter(adapter);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener((parent, view, position, id) ->
                selectItemFromDrawer(position));

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(v -> mDrawerLayout.openDrawer(GravityCompat.START));

        Fragment fragment = null;
        if (savedInstanceState != null) {
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, FRAGMENT_SAVED);
        }
        if (fragment == null) {
            selectItemFromDrawer(0);
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.mainContent, fragment, FRAGMENT_MAIN)
                    .commit();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, FRAGMENT_SAVED,
                Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag(FRAGMENT_MAIN)));
    }

    /*
     * Called when a particular item from the navigation drawer
     * is selected.
     * */
    private void selectItemFromDrawer(int position) {
        Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();

        String title = mNavItems.get(position).mTitle;

        if (title.equals(getString(R.string.disable_apps))) {
            fragment = new AppFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.mainContent, fragment, FRAGMENT_MAIN)
                    .commit();
        } else if (title.equals(getString(R.string.charge_limit))) {
            fragment = new ChargeFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.mainContent, fragment, FRAGMENT_MAIN)
                    .commit();
        } else {
            fragment = new AboutFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.mainContent, fragment, FRAGMENT_MAIN)
                    .commit();
        }

        mDrawerList.setItemChecked(position, true);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);

        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }

    class NavItem {
        String mTitle;
        String mSubtitle;
        int mIcon;

        NavItem(String title, String subtitle, int icon) {
            mTitle = title;
            mSubtitle = subtitle;
            mIcon = icon;
        }
    }

    class DrawerListAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<NavItem> mNavItems;

        DrawerListAdapter(Context context, ArrayList<NavItem> navItems) {
            mContext = context;
            mNavItems = navItems;
        }

        @Override
        public int getCount() {
            return mNavItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mNavItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert inflater != null;
                view = inflater.inflate(R.layout.drawer_item, parent, false);
            }
            else {
                view = convertView;
            }

            TextView titleView = view.findViewById(R.id.title);
            TextView subtitleView = view.findViewById(R.id.subTitle);
            ImageView iconView = view.findViewById(R.id.icon);

            titleView.setText(mNavItems.get(position).mTitle);
            subtitleView.setText(mNavItems.get(position).mSubtitle);
            iconView.setImageResource(mNavItems.get(position).mIcon);

            return view;
        }
    }
}
