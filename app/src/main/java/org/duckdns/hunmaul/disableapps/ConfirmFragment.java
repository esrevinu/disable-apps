package org.duckdns.hunmaul.disableapps;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;


public class ConfirmFragment extends DialogFragment {

    private static final String ARG_DIALOG_MESSAGE = "message";

    private ConfirmDialogListener mListener;

    public interface ConfirmDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
    }

    public static ConfirmFragment newInstance(String message, ConfirmDialogListener listener) {
        ConfirmFragment fragment = new ConfirmFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DIALOG_MESSAGE, message);
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    private void setListener(ConfirmDialogListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setMessage(Objects.requireNonNull(getArguments()).getString(ARG_DIALOG_MESSAGE))
                .setPositiveButton(R.string.confirm, (dialog, id) ->
                        mListener.onDialogPositiveClick(ConfirmFragment.this))
                .setNegativeButton(R.string.cancel, (dialog, id) ->
                        Toast.makeText(getContext(), getString(R.string.toast_cancel),
                        Toast.LENGTH_SHORT).show());
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
