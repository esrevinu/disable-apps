package org.duckdns.hunmaul.disableapps;

import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class MyAppRecyclerViewAdapter
        extends RecyclerView.Adapter<MyAppRecyclerViewAdapter.ViewHolder>
        implements Parcelable {

    public static final Creator<MyAppRecyclerViewAdapter> CREATOR = new Creator<MyAppRecyclerViewAdapter>() {
        @Override
        public MyAppRecyclerViewAdapter createFromParcel(Parcel in) {
            return new MyAppRecyclerViewAdapter(in);
        }

        @Override
        public MyAppRecyclerViewAdapter[] newArray(int size) {
            return new MyAppRecyclerViewAdapter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSparseBooleanArray(mSelectedItems);
        dest.writeSerializable(mMode);
    }

    public enum MODE { TO_DISABLE, TO_ENABLE }

    private final OnListInteractionListener mListener;
    private PackageManager mPM;
    private SparseBooleanArray mSelectedItems;
    private MODE mMode;
    private AppList mApps;

    MyAppRecyclerViewAdapter(AppList applist, OnListInteractionListener listener) {
        mSelectedItems = new SparseBooleanArray();
        mMode = MODE.TO_DISABLE;
        mListener = listener;
        mApps = applist;
    }

    private MyAppRecyclerViewAdapter(Parcel in) {
        mSelectedItems = in.readSparseBooleanArray();
        mMode = (MODE) in.readSerializable();
        mListener = null;
        mApps = AppList.getInstance(null);
    }

    MyAppRecyclerViewAdapter(MyAppRecyclerViewAdapter adapter,
                             OnListInteractionListener listener) {
        mSelectedItems = adapter.mSelectedItems;
        mMode = adapter.mMode;
        mListener = listener;
        mApps = AppList.getInstance(null);
    }

    void updateDataSet() {
        mApps = AppList.getInstance(null);
        mSelectedItems.clear();
        mApps.update();
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mPM = recyclerView.getContext().getPackageManager();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_app, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        AppList.App item = mApps.getSortedList().get(position);
        Glide.with((Fragment)mListener).load(item.icon).into(holder.mIconView);
        //holder.mIconView.setImageDrawable(mPM.getApplicationIcon(item));
        if (item.app_info.enabled) {
            holder.mAppNameView.setTextColor(0xFF7799EE);
        } else {
            holder.mAppNameView.setTextColor(0xFFCC3333);
        }
        holder.mAppNameView.setText(item.name);
        holder.mPackageNameView.setText(item.app_info.packageName);
        if (mSelectedItems.get(position, false)) {
            holder.mView.setEnabled(true);
            holder.mView.setSelected(true);
        } else {
            holder.mView.setSelected(false);
            if ((mMode == MODE.TO_DISABLE && !item.app_info.enabled) ||
                    (mMode == MODE.TO_ENABLE && item.app_info.enabled)) {
                if (mSelectedItems.size() != 0)
                    holder.mView.setEnabled(false);
                else
                    holder.mView.setEnabled(true);
            } else {
                holder.mView.setEnabled(true);
            }
        }

        holder.mView.setOnClickListener(v -> {
            if (mSelectedItems.get(position, false)) {
                mSelectedItems.delete(position);
            } else {
                if (mSelectedItems.size() == 0) {
                    mMode = item.app_info.enabled ? MODE.TO_DISABLE : MODE.TO_ENABLE;
                }
                mSelectedItems.put(position, true);
            }
            notifyDataSetChanged();

            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onListInteraction(item, mMode,
                    mSelectedItems.get(position, false));
        });
    }

    @Override
    public int getItemCount() {
        if (mApps == null) return 0;
        else return mApps.getSortedList().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mIconView;
        final TextView mAppNameView;
        final TextView mPackageNameView;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mIconView = view.findViewById(R.id.appicon);
            mAppNameView = view.findViewById(R.id.appname);
            mPackageNameView = view.findViewById(R.id.packagename);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mPackageNameView.getText() + "'";
        }
    }

    public interface OnListInteractionListener {
        void onListInteraction(@Nullable AppList.App item,
                               MyAppRecyclerViewAdapter.MODE mode,
                               boolean selected);
    }
}
