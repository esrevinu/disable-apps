package org.duckdns.hunmaul.disableapps;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

class SocketWork {

    private static final String COMM_DISABLE_APPS = "disable-apps";
    private static final String COMM_ENABLE_APPS  = "enable-apps";
    private static final String COMM_ADJUST_CHARGE_LIMIT = "adjust-charge-limit";
    private static final String COMM_REQUEST_CHARGE_LIMIT = "request-charge-limit";

    private static final LocalSocketAddress socketAddress
            = new LocalSocketAddress("disable_apps_socket");

    private LocalSocket socket;

    SocketWork() {
        socket = new LocalSocket();
    }

    private Timer putInputTimeout(int delay) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    socket.shutdownInput();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, delay);

        return timer;
    }

    private OutputStream connect() {
        try {
            socket.connect(socketAddress);
            if (socket.isConnected()) {
                return socket.getOutputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    String sendDisableEnableApps(boolean disable, List<String> packagenames) {
        OutputStream os;
        if ((os = connect()) != null) {
            OutputStreamWriter out = new OutputStreamWriter(os);
            try {
                if (disable) {
                    out.append(COMM_DISABLE_APPS).append('\n');
                } else {
                    out.append(COMM_ENABLE_APPS).append('\n');
                }
                for (String name: packagenames) {
                    out.append(name).append('\n');
                }
                out.append(".\n");
                out.flush();

                Timer timer = putInputTimeout(4000 + packagenames.size() * 1000);

                InputStreamReader in = new InputStreamReader(socket.getInputStream());
                String result = new BufferedReader(in).readLine();

                if (result == null) {
                    result = "Timeout";
                }

                out.close();
                in.close();
                socket.close();
                timer.cancel();

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return "IOException";
            }
        }
        return "Connection failed";
    }

    String sendChageLimitSettings(int llk_enable, int max, int min) {
        OutputStream os;
        if ((os = connect()) != null) {
            OutputStreamWriter out = new OutputStreamWriter(os);
            try {
                out.append(COMM_ADJUST_CHARGE_LIMIT).append('\n');

                out.append(Integer.toString(llk_enable)).append(' ')
                        .append(Integer.toString(max)).append(' ')
                        .append(Integer.toString(min)).append('\n');

                out.flush();

                Timer timer = putInputTimeout(5000);

                InputStreamReader in = new InputStreamReader(socket.getInputStream());
                String result = new BufferedReader(in).readLine();

                if (result == null) {
                    result = "Timeout";
                }

                out.close();
                in.close();
                socket.close();
                timer.cancel();

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return "IOException";
            }
        }
        return "Connection failed";
    }

    String requestChageLimitSettings() {
        OutputStream os;
        if ((os = connect()) != null) {
            OutputStreamWriter out = new OutputStreamWriter(os);
            try {
                out.write(COMM_REQUEST_CHARGE_LIMIT);
                out.append('\n');

                out.flush();

                Timer timer = putInputTimeout(5000);

                InputStreamReader in = new InputStreamReader(socket.getInputStream());
                String result = new BufferedReader(in).readLine();

                if (result == null) {
                    result = "Timeout";
                }

                out.close();
                in.close();
                socket.close();
                timer.cancel();

                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return "IOException";
            }
        }
        return "Connection failed";
    }
}
