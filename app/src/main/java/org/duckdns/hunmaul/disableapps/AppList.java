package org.duckdns.hunmaul.disableapps;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import androidx.recyclerview.widget.SortedList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppList {

    private static AppList sInstance;
    private static SortedList<App> app_list;
    private PackageManager mPM;

    public static AppList getInstance(PackageManager pm) {
        if (sInstance == null) {
            synchronized (AppList.class) {
                if (sInstance == null) {
                    sInstance = new AppList(pm);
                }
            }
        }
        return sInstance;
    }

    private AppList(PackageManager pm) {
        mPM = pm;
        app_list = new SortedList<>(App.class, new SortedList.Callback<App>() {
            @Override
            public int compare(App o1, App o2) {
                return o1.name.compareTo(o2.name);
            }

            @Override
            public void onChanged(int position, int count) {

            }

            @Override
            public boolean areContentsTheSame(App oldItem, App newItem) {
                return oldItem.app_info.enabled == newItem.app_info.enabled;
            }

            @Override
            public boolean areItemsTheSame(App item1, App item2) {
                return item1.app_info.packageName.equals(item2.app_info.packageName);
            }

            @Override
            public void onInserted(int position, int count) {

            }

            @Override
            public void onRemoved(int position, int count) {

            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {

            }
        });
        Iterator<ApplicationInfo> it = pm.getInstalledApplications(0)
                .stream().filter(p -> (p.flags & ApplicationInfo.FLAG_SYSTEM) == 0)
                .iterator();
        while (it.hasNext()) {
            app_list.add(new App(it.next()));
        }
    }

    public void update() {
        Iterator<ApplicationInfo> it = mPM.getInstalledApplications(0)
                .stream().filter(p -> (p.flags & ApplicationInfo.FLAG_SYSTEM) == 0)
                .iterator();
        List<App> new_list = new ArrayList<>();
        while (it.hasNext()) {
            new_list.add(new App(it.next()));
        }

        app_list.replaceAll(new_list);
    }


    public SortedList<App> getSortedList() {
        return app_list;
    }

    public class App {
        String name;
        Drawable icon;
        ApplicationInfo app_info;

        App(ApplicationInfo a) {
            app_info = a;
            name = mPM.getApplicationLabel(a).toString().replace('\n', ' ');
            icon = mPM.getApplicationIcon(a);
        }
    }

}
